# NgxFormlyDemo
1) basic form - text controls
2) dropdowns - simple and get data from backend
3) cascading dropdowns
4) hiding and disabling stuff
5) basic validation
6) custom validators - heirarchy.. customm messages
7) some other components 


Validations
1) ways - some validations can be added within template options


--------
https://juristr.com/blog/2019/12/ng-be-2019-formly-talks/

--------
Problems
1) styling.. lets say we want 2 controls in one row ?
2) some specific weird behaviour
3) what are all the types - lets say i want to enter phone number... or any specific type corresponding to a component library ?
4) adding a hook to an invisible control
5) formly config csan get big - create methods to return formly field config 
6) button inside
7) flex
