import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.scss'],
  // encapsulation: ViewEncapsulation.None,
})
export class FormGroupComponent implements OnInit {
  signupForm = new FormGroup({});

  formModel = {     // data model
    username: null,
    passwordGroup: {
      password: null,
      confirmPassword: null
    }
  };

  formFields: FormlyFieldConfig[] = [
    {
      key: 'username',
      type: 'input',  //one type here
      templateOptions: {
        type: 'text',
        label: 'Full Name',
        required: true
      }
    },
    {
      key: 'passwordGroup',
      fieldGroup: [
        {
          key: 'password',
          type: 'input',  //one type here
          templateOptions: {
            type: 'password',
            label: 'Password',
            required: true
          }
        },
        {
          key: 'confirmPassword',
          type: 'input',  //one type here
          templateOptions: {
            type: 'password',
            label: 'Confirm password',
            required: true
          }
        }
      ],
      validators: {
        fieldMatch: {
          expression: (group) => {
            const value = group.value;
            return value.confirmPassword === value.password
              // avoid displaying the message error when values are empty
              || (!value.confirmPassword || !value.password);
          },
          message: 'Password Not Matching',
          errorPath: 'confirmPassword',
        },
      }
    },
    {
      fieldGroupClassName: 'form-row',
      //className: 'form-row',
      fieldGroup: [
        {
          className: 'form-row-item',
          type: 'input',
          key: 'cityName',
          templateOptions: {
            label: 'City',
          },
        },
        {
          className: 'form-row-item',
          type: 'input',
          key: 'zip',
          templateOptions: {
            type: 'number',
            label: 'Zip',
            max: 99999,
            min: 0,
            pattern: '\\d{5}',
          },
        },
      ]
    }

  ];

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit({valid, value}) {
    console.table(value);
  }

}
