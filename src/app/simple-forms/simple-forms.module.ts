import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleFormOneComponent } from './simple-form-one/simple-form-one.component';
import {SimpleFormsRoutingModule} from './simple-forms-routing.module';
import {FormlyFieldConfig, FormlyModule} from '@ngx-formly/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { FormGroupComponent } from './form-group/form-group.component';
import { FormWithFlexLayoutComponent } from './form-with-flex-layout/form-with-flex-layout.component';

export function minValidationMsg(err, field: FormlyFieldConfig) {
  return `Global error msg, has to be at least ${err.min}. You input ${err.actual}...`;
}

@NgModule({
  declarations: [SimpleFormOneComponent, FormGroupComponent, FormWithFlexLayoutComponent],
  imports: [
    CommonModule,
    SimpleFormsRoutingModule,
    FormlyModule.forChild({
      validationMessages: [
        {
          name: 'required',
          message: 'This is a required field...'
        },
        {
          name: 'min',
          message: minValidationMsg
        }
      ]
    }),
    ReactiveFormsModule,
    MatButtonModule
  ]
})
export class SimpleFormsModule { }
