import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormWithFlexLayoutComponent } from './form-with-flex-layout.component';

describe('FormWithFlexLayoutComponent', () => {
  let component: FormWithFlexLayoutComponent;
  let fixture: ComponentFixture<FormWithFlexLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormWithFlexLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormWithFlexLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
