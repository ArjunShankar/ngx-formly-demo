import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SimpleFormOneComponent } from './simple-form-one/simple-form-one.component';
import { FormGroupComponent } from './form-group/form-group.component';
import {FormWithFlexLayoutComponent} from './form-with-flex-layout/form-with-flex-layout.component';


const childRoutes: Routes = [
  { path: 'simple-form', component: SimpleFormOneComponent },
  { path: 'form-group', component: FormGroupComponent },
  { path: 'form-with-layout', component: FormWithFlexLayoutComponent } ,
  { path: '', redirectTo: 'simple-form', pathMatch: 'full'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule]
})
export class SimpleFormsRoutingModule { }

