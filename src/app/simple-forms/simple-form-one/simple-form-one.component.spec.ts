import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleFormOneComponent } from './simple-form-one.component';

describe('SimpleFormOneComponent', () => {
  let component: SimpleFormOneComponent;
  let fixture: ComponentFixture<SimpleFormOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleFormOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleFormOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
