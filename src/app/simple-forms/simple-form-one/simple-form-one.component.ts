import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import {DataService} from '../../core/services/data.service';
import {switchMap, takeUntil, tap} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-simple-form-one',
  templateUrl: './simple-form-one.component.html',
  styleUrls: ['./simple-form-one.component.scss']
})
export class SimpleFormOneComponent implements OnInit {
  simpleFormOne = new FormGroup({});
  subs: Subscription = new Subscription();
  private objectList = [
    { label: 'Orgz', value: 'orgz', isHierarchical: true },
    { label: 'Groups', value: 'grps', isHierarchical: true  },
    { label: 'Employeed', value: 'employee', isHierarchical: false  },
  ];
  formModel = {     // data model
    id: '12891829182912',
    name: 'Arj',
    age: 33,
    occupationId: 8,
    industryId: null
  };
  formFields: FormlyFieldConfig[] = [
    {
      key: 'id'     // hidden form field
    },
    {
      key: 'name',
      type: 'input',  //one type here
      templateOptions: {
        type: 'text',
        label: 'Full Name',
        required: true
      }
    },
    {
      key: 'age',
      type: 'input',
      templateOptions: {
        type: 'number', //another type inside the template options
        label: 'Age(yrs)',
        required: true,
        min: 18,
        max: 90
      },
      validation: {
        // messages: {
        //   min: 'Local error msg.... put in between 10-90'
        // }
      }
    },
    {
      key: 'occupationId',
      type: 'select',
      templateOptions: {
        label: 'Select Occupation',
        /*options: [
          { label: 'Code Monkey', value: 9 },
          { label: 'Zoo keeper', value: 98 },
        ],*/
        options: this.dataService.getOccupationOptions(),
      }
    },
    {
      key: 'industryId',
      type: 'select',
      templateOptions: {
        label: 'Select the industry',
        options: [
          { value: '1', label: 'Soccer' },
          { value: '2', label: 'Basketball' },
        ]
      },
      hooks: {
        onInit: (field: FormlyFieldConfig) => {
          field.templateOptions.options = field.form.get('occupationId').valueChanges.pipe(
            // tap((occupationId) => console.log('jjk', occupationId)),
            switchMap((occupationId) => this.dataService.getIndustryOptions(occupationId))
          );
        }
      },
      expressionProperties: {   // this can be done via hooks as well
        'templateOptions.disabled': (model) => {
          return model.occupationId === -1;
        }
      },
      hideExpression: (model) => {  // hiding a form control .. this also works with a string expressipon
        return model.occupationId === -1;
      }
      // hooks: {
      //   onInit: field => {
      //     field.formControl.valueChanges.pipe(
      //       tap(value => {
      //         console.warn('***', value);
      //       }),
      //     ).subscribe();
      //   },
      // }
    },
    {
      key: 'objectType',
      type: 'select',
      templateOptions: {
        label: 'Select object type',
        options: this.objectList,
      }
    },
    {
      key: 'objectViewType',
      type: 'select',
      templateOptions: {
        label: 'Select object view type',
        options: [
          { label: 'flat', value: 'flat' },
          { label: 'hierarchy', value: 'hierarchy'  },
        ],
      },
      hooks: {
        onInit: (field: FormlyFieldConfig) => {
         this.subs.add(
         field.form.get('objectType').valueChanges.pipe(
             tap(() => field.formControl.setValue(null)),
          ).subscribe());
        },
        onDestroy: field => {
          console.log('IN UNSUB ');
          this.subs.unsubscribe();
        }
      },
      hideExpression: (model) => {  // hiding a form control .. this also works with a string expressipon
        console.log('in hide expression::', model.objectType);
        return model.objectType == null;
      }
    }
  ];
  constructor(
    private dataService: DataService
  ) { }

  ngOnInit() {
  }

  onSubmit({valid, value}) {
    console.table(value);
  }

}
