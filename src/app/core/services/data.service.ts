import { Injectable } from '@angular/core';
import {from, Observable, of} from 'rxjs';
import {filter} from 'rxjs/operators';

export interface IOccupation {
  label: string;
  value: number;
}

export interface IIndustryOn {
  label: string;
  value: number;
  occupationId: number;
}

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getOccupationOptions(): Observable<IOccupation[]> {
    return of([
      { label: 'NA', value: -1 },
      { label: 'Code Monkey', value: 9 },
      { label: 'Zoo keeper', value: 98 },
      { label: 'Time traveller', value: 2 },
      { label: 'Botanist', value: 78 },
    ]);
  }

  getIndustryOptions(inputId): Observable<IIndustryOn[]> {
    console.log(inputId);
    return of([
      { label: 'Big oil', value: 1, occupationId: 78 },
      { label: 'Big Pharma', value: 2, occupationId: 98  },
      { label: 'Big Tech', value: 3, occupationId: 9  },
      { label: 'Big B', value: 2, occupationId: 2  },
      { label: 'Big C', value: 3, occupationId: 2  }
    ].filter((industry: IIndustryOn) => industry.occupationId === inputId));
  }

}
