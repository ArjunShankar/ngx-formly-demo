import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './component/layout.component';

const childRoutes: Routes = [
  { path: '', component: LayoutComponent,
    children: [
      { path: 'formly-forms', loadChildren: () => import('../simple-forms/simple-forms.module').then(m => m.SimpleFormsModule) },
      { path: '', redirectTo: 'formly-forms', pathMatch: 'full'}
    ]},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(childRoutes)
  ],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
